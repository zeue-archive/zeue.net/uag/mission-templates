if ((!isServer) && (player != player)) then {waitUntil {player == player};};

if (!isDedicated && hasInterface) then {
	player setUnitTrait ["Medic",true];
};

enableSentences false;

#include "ares_custom.hpp"
