<?php

	if (isset($_GET['ln'])) {

		$servername = "HOST";
		$username   = "USER";
		$password   = "PASS";
		$dbname     = "DB";

		$conn = new mysqli($servername, $username, $password, $dbname);
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		$sql    = "SELECT loadout FROM arsenal WHERE code = '" . $_GET['ln'] . "'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				echo $row["loadout"];
			}
		} else { echo "hint 'Invalid loadout ID'"; }
		
		$conn->close();
		
	} else { echo "hint 'No loadout ID provided, closing.'"; }
	
?>